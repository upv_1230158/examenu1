package upv.com.myapplication;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


public class MainActivity extends ActionBarActivity {

    //Declarar los elementeos
   private EditText et1,et2;
   private Button btnCalular;
   private TextView tvResultado,tvangulo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        et1 = (EditText) findViewById(R.id.et1);
        et2 = (EditText) findViewById(R.id.et2);
        btnCalular = (Button) findViewById(R.id.btnCalcular);
        tvResultado=(TextView) findViewById(R.id.tvResultado);
        tvangulo=(TextView) findViewById(R.id.tvangulo);

    }

    public void Calculando(View view){
        int x= Integer.valueOf(et1.getText().toString());
        int y= Integer.valueOf(et2.getText().toString());
        double r = Math.hypot(x, y);
        //double r = (x*x)+(y*y);

        double aRad = Math.atan2(y, x);
        double a = Math.toDegrees(aRad);

        //double raiz = Math.sqrt(r);
        //double res = Math.toDegrees(y/x);

        tvResultado.setText("Radio: "+ aRad );
        tvangulo.setText("Angulo: "+a+" grados");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
